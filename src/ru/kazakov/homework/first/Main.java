package ru.kazakov.homework.first;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Main {

    static Logger logger = Logger.getLogger(Main.class.getName());

    public static void main(String[] args) throws AWTException {
        logger.log(Level.INFO, "Кофе-машина");
        logger.log(Level.INFO, "Введите сумму");
        Scanner scanner = new Scanner(System.in);
        int moneyAmount = scanner.nextInt();
        int[] drinkPrices = {165, 100, 70, 80};
        String[] drinkNames = {"капучино", "эспрессо", "эспрессо", "латте"};
        boolean canBuySomething = false;
        for (int i = 0; i < drinkPrices.length; i++) {
            if (moneyAmount >= drinkPrices[i]) {
                logger.log(Level.INFO, "Вы можете купить " +
                        drinkNames[i] + " по цене " +
                        drinkPrices[i] + " р.");
                canBuySomething = true;
            }
        }
        if (!canBuySomething) {
            logger.log(Level.WARNING, "Недостаточно средств");
        }
        SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd_HHmmss");
        Date now = new Date();
        logger.log(Level.INFO, "Текущая дата: {0}", formatter.format(now));
        BufferedImage image = new Robot().createScreenCapture(new Rectangle(Toolkit.getDefaultToolkit().getScreenSize()));
        logger.log(Level.INFO, image.getWidth() + "x" + image.getHeight());
    }

}
