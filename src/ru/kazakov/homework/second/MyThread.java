package ru.kazakov.homework.second;

import com.dropbox.core.v2.DbxClientV2;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

public class MyThread extends Thread {

    private final DbxClientV2 dbClient;

    private final BufferedImage screen;

    private final SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd_HHmmss");

    public MyThread(DbxClientV2 client, BufferedImage image) {
        this.dbClient = client;
        this.screen = image;
    }

    @Override
    public void run() {
        try {
            ByteArrayOutputStream os = new ByteArrayOutputStream();
            ImageIO.write(screen, "png", os); // Passing: ​(RenderedImage im, String formatName, OutputStream output)
            InputStream in = new ByteArrayInputStream(os.toByteArray());
            String fileName = formatter.format(new Date()) + ".png";
            dbClient.files().uploadBuilder("/" + fileName)
                    .uploadAndFinish(in);
        } catch (Exception e) {
            e.printStackTrace();
            Thread.currentThread().interrupt();
        }

    }
}
