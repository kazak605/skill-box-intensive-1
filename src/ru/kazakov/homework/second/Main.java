package ru.kazakov.homework.second;

import com.dropbox.core.DbxRequestConfig;
import com.dropbox.core.v2.DbxClientV2;

import java.awt.*;
import java.awt.image.BufferedImage;

public class Main {

    private static final String ACCESS_TOKEN = "G4CUzONgm6wAAAAAAAAAARFOyNuvKoF5DleOtWReBLS3q2Dg70UyL4SGJZ84yGRp";

    private static final DbxRequestConfig config = DbxRequestConfig.newBuilder("dropbox/java-tutorial").build();

    private static final DbxClientV2 client = new DbxClientV2(config, ACCESS_TOKEN);

    public static void main(String[] args) {

        for (; ; ) {
            try {
                BufferedImage image = new Robot().createScreenCapture(new Rectangle(Toolkit.getDefaultToolkit().getScreenSize()));
                MyThread myThread = new MyThread(client, image);
                myThread.start();
                Thread.sleep(5000);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

}
